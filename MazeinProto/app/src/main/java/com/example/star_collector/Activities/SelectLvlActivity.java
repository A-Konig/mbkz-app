package com.example.star_collector.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.star_collector.Model.Utils;
import com.example.star_collector.R;

/**
 * Activity controlling the level selection layout
 *
 * @author Alex Konig
 */
public class SelectLvlActivity extends AppCompatActivity {

    /** Last opened level */
    int lastOpened;
    /** Buttons for the selection of levels */
    Button[] levelBTNS;

    /**
     * Called upon resuming the activity
     */
    @Override
    protected void onResume() {
        super.onResume();

        // reload open levels
        lastOpened = Utils.loadLevel();
        for (int i = 0; i < 4; i++) {
            if (i < lastOpened) {
                levelBTNS[i].setEnabled(true);
            } else {
                levelBTNS[i].setEnabled(false);
            }
        }
    }

    /**
     * Called on creation of the layout
     * @param savedInstanceState    saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_lvl);

        // fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        // get last opened level
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            lastOpened = extras.getInt("OPENED");
        }


        // level select buttons
        levelBTNS = new Button[4];
        levelBTNS[0] = findViewById(R.id.lvl1BT);
        levelBTNS[1] = findViewById(R.id.lvl2BT);
        levelBTNS[2] = findViewById(R.id.lvl3BT);
        levelBTNS[3] = findViewById(R.id.lvl4BT);

        for (int i = lastOpened; i < 4; i++) {
            levelBTNS[i].setEnabled(false);
        }

        // background
        View view =  this.getWindow().getDecorView();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        if(width>1600)
            view.setBackgroundResource (R.drawable.lvlbg_xxxhdpi);
        else if(width>960)
            view.setBackgroundResource(R.drawable.lvlbg_xxhdpi);
        else if(width>800)
            view.setBackgroundResource(R.drawable.lvlbg_xhdpi);
        else if(width>480)
            view.setBackgroundResource(R.drawable.lvlbg_hdpi);
        else if (width>320)
            view.setBackgroundResource(R.drawable.lvlbg_mdpi);
        else
            view.setBackgroundResource(R.drawable.lvlbg_ldpi);
    }

    /**
     * Start game activity
     * @param v view
     */
    public void StartGame(View v) {
        Intent i = new Intent(this,  GameActivity.class);

        // selected level
        int value = 0;
        switch (v.getId()) {
            // level 1
            case R.id.lvl1BT:
                value = 1;
                break;
            // level 2
            case R.id.lvl2BT:
                value = 2;
                break;
            // level 3
            case R.id.lvl3BT:
                value = 3;
                break;
            // level 4
            case R.id.lvl4BT:
                value = 4;
                break;
        }

        // put extra
        i.putExtra("LVL", value);
        i.putExtra("TIME", 90);

        startActivity(i);
    }

    /**
     * Go back
     * @param v view
     */
    public void back(View v) {
        onBackPressed();
    }

    /**
     * Called when back button pressed
     */
    @Override
    public void onBackPressed() {
        finish();
    }
}
