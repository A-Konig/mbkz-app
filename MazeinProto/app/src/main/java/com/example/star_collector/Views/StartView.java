package com.example.star_collector.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.example.star_collector.R;

/**
 * Class used in AboutActivity layout for the display of the start of the maze
 *
 * @author Alex Konig
 */
public class StartView extends View {

    /** Color of start */
    private Paint start;

    /** Width of view */
    private int width;
    /** Height of view */
    private int height;

    /** Width of stroke */
    private int strokeWidth = 5;
    /** Default size of one cell in maze */
    private int cellSize = 100;

    /**
     * Constructor
     * @param context context
     */
    public StartView(Context context) {
        this(context, null);
    }

    /**
     * Constructor that sets the colour of start
     * @param c context
     * @param attrs attributes
     */
    public StartView(Context c, AttributeSet attrs)
    {
        super(c, attrs);
        start = new Paint();
        start.setColor(ContextCompat.getColor(c, R.color.StartBlue));
    }

    /**
     * Reaction on size changed
     * @param w     new widht
     * @param h     new height
     * @param oldw  old width
     * @param oldh  old height
     */
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width = w;
        this.height = h;
    }

    /**
     * Method that draws contents of the view
     * @param canvas    canvas
     */
    protected void onDraw(Canvas canvas) {
        drawPortal(canvas, start, width/2 - cellSize/2, height/2 - cellSize/2);
    }

    /**
     * Draw portal representing the end or start of a maze
     * @param canvas    canvas
     * @param colour    color of portal
     * @param x         x coordinate of the left top corner of cell
     * @param y         y coordinate of the left top corner of cell
     */
    private void drawPortal(Canvas canvas, Paint colour, int x, int y) {
        canvas.drawRect(x + strokeWidth * 1.5f, y + strokeWidth * 1.5f, x + cellSize - strokeWidth, y + cellSize - strokeWidth, colour);
    }

}
