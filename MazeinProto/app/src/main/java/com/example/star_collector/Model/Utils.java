package com.example.star_collector.Model;

public class Utils {

    /** Last open level in game, default set on 1 */
    private static int openLvl = 1;

    /**
     * Create a string from a given time in seconds
     * @param time  time in seconds
     * @return      string representing time in format hh:mm:ss
     */
    public static String createTimeString(int time) {
        int min = time / 60;
        int sec = time % 60;

        String minStr = ""+min;
        String secStr = ""+sec;

        if (min < 10)
            minStr = "0" + minStr;
        if (sec < 10)
            secStr = "0"+secStr;

        return "00:" + minStr + ":" + secStr;
    }

    /**
     * Setter for stored last open level value
     * @param value new value
     */
    public static void changeLevel(int value) {
        openLvl = value;
    }

    /**
     * Getter for stored last open level value
     * @return  last open level value
     */
    public static int loadLevel() {
        return openLvl;
    }

}
