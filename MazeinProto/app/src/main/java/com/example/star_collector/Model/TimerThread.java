package com.example.star_collector.Model;

import android.widget.TextView;

import com.example.star_collector.Activities.GameActivity;

/**
 * Timer thread that every second decreases remaining time to solve mazes by one second
 *
 * @author Alex Konig
 */
public class TimerThread implements Runnable {

    /** Remaining time */
    private int time;
    /** TextView in which is the time displayed */
    private TextView display;
    /** Activity in which is time displayed */
    private GameActivity activity;

    /**
     * Constructor
     * @param timeInSeconds starting time in seconds
     * @param display       textview displaying the time
     * @param activity      activity displaying the time
     */
    public TimerThread(int timeInSeconds, TextView display, GameActivity activity) {
        time = timeInSeconds;
        this.display = display;
        this.activity = activity;
    }

    /**
     * Run thread
     */
    @Override
    public void run() {
        // add time bonus
        if (activity.addTime) {
            activity.addTime = false;

            if (activity.finishedLvls%5 == 0)
                time += 10;
            else
                time += 2;

        // activity not paused
        } else if (!activity.paused)
            time--;

        // game over
        if (time == -1) {
            activity.ExitTimeMode();
        }

        // display new time
        display.setText(Utils.createTimeString(time));
    }

}