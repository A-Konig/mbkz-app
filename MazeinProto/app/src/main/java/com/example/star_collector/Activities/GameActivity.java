package com.example.star_collector.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.star_collector.Model.Maze;
import com.example.star_collector.Model.MazeController;
import com.example.star_collector.Model.TimerThread;
import com.example.star_collector.Model.Utils;
import com.example.star_collector.R;
import com.example.star_collector.Views.GameView;

import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Activity controlling the game layout
 *
 * @author Alex Konig
 */
public class GameActivity extends AppCompatActivity {

    /** Game view */
    GameView gameView;
    /** Maze controller */
    MazeController gameControl;
    /** Maze */
    Maze game;

    /** Timer thread */
    Thread thread;

    /** Selected level */
    int level;
    /** Number of finished mazes */
    public int finishedLvls;

    /** Textview displaying collected stars */
    TextView collected;
    /** Timer */
    TextView timer;

    /** Should time be added */
    public boolean addTime = false;
    /** Is game paused */
    public boolean paused = false;

    /**
     * Called on creation of the layout
     * @param savedInstanceState    saved instance
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        // get mode and level
        level = 1;
        int time = 90;
        finishedLvls = 0;
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            level = extras.getInt("LVL");
            time = extras.getInt("TIME");
        }

        // new maze
        game = new Maze(level);
        gameControl = new MazeController(game, level);
        gameControl.createMaze();
        gameView = findViewById(R.id.view);
        gameView.setGame(game);

        // set collected text
        collected = findViewById(R.id.textView2);

        // set timer text
        timer = findViewById(R.id.timerTxt);
        timer.setText("");

        // if time mode
        if (level == -1) {
            // set text
            String finTXT = "Finished: " + finishedLvls;
            collected.setText(finTXT);
            timer.setText(Utils.createTimeString(time));

            // start timer
            final TimerThread th = new TimerThread(time, timer, this);
            thread = new Thread() {
                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {
                            Thread.sleep(1000);
                            runOnUiThread(th);
                        }
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            };
            thread.start();
        }
    }

    /**
     * Move in maze and collect stars if neccessary, called by move buttons
     * @param v view
     */
    public void mazeMovement(View v) {
        String collTXT = "Collected: ";
        switch (v.getId()) {
            // up
            case R.id.upBT:
                gameControl.movePlayer(5);
                if (level != -1) {
                    collTXT += gameControl.collected;
                    collected.setText(collTXT);
                }
                break;
            // left
            case R.id.leftBT:
                gameControl.movePlayer(1);
                if (level != -1) {
                    collTXT += gameControl.collected;
                    collected.setText(collTXT);
                }
                break;
            // right
            case R.id.rightBT:
                gameControl.movePlayer(3);
                if (level != -1) {
                    collTXT += gameControl.collected;
                    collected.setText(collTXT);
                }
                break;
            // down
            case R.id.downBT:
                gameControl.movePlayer(2);
                if (level != -1) {
                    collTXT += gameControl.collected;
                    collected.setText(collTXT);
                }
                break;
        }

        // moved on exit
        if ( (game.playerY == game.endY) && (game.playerX == game.endX) && (game.open) ) {
            // if level finished
            if (!gameControl.createMaze()) {
                beatenLevel();
            }

            // if time mode
            if (level == -1) {
                finishedLvls++;
                addTime = true;
                String finTXT = "Finished: " + finishedLvls;
                collected.setText(finTXT);
                gameView.onSizeChanged(gameView.getWidth(), gameView.getHeight(), gameView.getWidth(), gameView.getHeight());
            }
        }

        // invalide so it will be repainted
        gameView.invalidate();
    }

    /**
     * Finished level, display dialog
     */
    private void beatenLevel() {
        if (level != -1) {
            saveOpenLevel();

            // show dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You beat the level").setTitle("CONGRATULATIONS");
            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    finish();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    /**
     * Save level as last open to disk if bigger than the saved value
     */
    private void saveOpenLevel() {
        FileInputStream fis;
        int saved;
        byte [] buffer = new byte[1024];
        String FILENAME = "LastOpen.txt";
        try {
            fis= openFileInput(FILENAME);
            fis.read(buffer, 0, 4);
            fis.close();
            saved = Integer.parseInt(""+((new String(buffer)).charAt(0)));
        } catch (Exception e) {
            saved = 1;
        }

        // is bigger
        if (saved > (level+1))
            return;

        // save
        FileOutputStream fos;
        String retez = "" + (level+1);
        try {
            fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(retez.getBytes());
            fos.close();
        } catch (Exception e) { System.out.println("CHYBA PŘI ZAPISOVÁNÍ"); }

        Utils.changeLevel(level + 1);
    }

    /**
     * Go back
     * @param v view
     */
    public void back(View v) {
        onBackPressed();
    }

    /**
     * Called upon the back button pressed
     * Display a dialog with warning
     */
    @Override
    public void onBackPressed() {
        // pause game
        paused = true;

        // create dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to quit? All progress will be lost.").setTitle("WARNING").setIcon(android.R.drawable.ic_dialog_alert);

        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if exiting time mode
                if (level == -1) {
                    saveFinishedMazes();
                    thread.interrupt();
                }
                finish();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                paused = false;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * Save number of finished mazes in time mode
     */
    private void saveFinishedMazes() {
        FileInputStream fis;
        int[] saved = {0, 0, 0};
        byte [] buffer = new byte[1024];
        String FILENAME = "Highscores.txt";
        try {
            fis= openFileInput(FILENAME);
            fis.read(buffer, 0, 1024);
            fis.close();

            String line = new String(buffer);

            String[] vals = line.split(" ");
            saved[0] = Integer.parseInt(vals[0]);
            saved[1] = Integer.parseInt(vals[1]);
            saved[2] = Integer.parseInt(vals[2]);
        } catch (Exception e) {
            saved[0] = 0;
        }

        // should new value be saved
        for (int i = 0; i < 3; i++) {
            if (saved[i] == finishedLvls) {
                return;
            }
        }

        // find space for it
        int index = 3;
        if (saved[0] < finishedLvls) {
            index = 0;
        } else if (saved[1] < finishedLvls) {
            index = 1;
        } else if (saved[2] < finishedLvls) {
            index = 2;
        }

        if (index > 2) {
            return;
        }

        // put into array
        int val = finishedLvls;
        for (int i = index; i < 3; i++) {
            int pom = saved[i];
            saved[i] = val;
            val = pom;
        }

        // create string to save
        String toSave = "";
        for (int i = 0; i < 3; i++) {
            toSave += saved[i] + " ";
        }

        // save
        FileOutputStream fos;
        try {
            fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(toSave.getBytes());
            fos.close();
        } catch (Exception e) { System.out.println("CHYBA PŘI ZAPISOVÁNÍ"); }

    }

    /**
     * Exit time mode game after time runs out
     */
    public void ExitTimeMode() {
        // save results
        saveFinishedMazes();
        thread.interrupt();

        // display dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You ran out of time after finishing " + finishedLvls + " mazes.").setTitle("GAME OVER").setIcon(android.R.drawable.ic_dialog_alert);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
}
