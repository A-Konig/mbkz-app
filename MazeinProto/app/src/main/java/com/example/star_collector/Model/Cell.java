package com.example.star_collector.Model;

/**
 * One cell in the maze
 * 
 * @author Alex Konig
 */
public class Cell {

	/** North wall */
	public boolean north;
	/** West wall */
	public boolean west;

	/** Is player present */
	public boolean player;
	/** Has cell been visited */
	boolean visited;
	/** Is there a collectible */
	public boolean collectible;
	
	/** X coordinate */
	int x;
	/** Y coordinate */
	int y;
	
	/**
	 * Creates new instance of Cell
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
		
		north = false; west = false;
		player = false; collectible = false;
		visited = false;
	}
	
}
