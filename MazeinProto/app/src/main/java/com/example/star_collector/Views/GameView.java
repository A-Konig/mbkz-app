package com.example.star_collector.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.example.star_collector.Model.Cell;
import com.example.star_collector.Model.Maze;
import com.example.star_collector.R;

/**
 * Class used in GameActivity layout for the display of the current state of played maze
 *
 * @author Alex Konig
 */
public class GameView extends View {

    /** Colours used */
    private Paint wall, background, character, collectible, exit, start;
    /** Stroke width */
    private int strokeWidth = 5;
    /** Adjust multiplicator for better centering */
    private float adjust = 1.5f;

    /** Maze to display */
    private Maze maze;
    /** Default size of cell in maze */
    int cellSize = 100;

    /** Width of view */
    private int width;
    /** Height of view */
    private int height;

    /**
     * Constructor
     * @param context context
     */
    public GameView(Context context) {
        this(context, null);
    }

    /**
     * Constructor that creates paints that will be used
     * @param c context
     * @param attrs attributes
     */
    public GameView(Context c, AttributeSet attrs)
    {
        super(c, attrs);

        // create paints that will be used
        background = new Paint();
        background.setColor(ContextCompat.getColor(c, R.color.PlayBG));

        wall = new Paint();
        wall.setColor(Color.BLACK);
        wall.setStrokeWidth(strokeWidth);

        character = new Paint();
        character.setColor(ContextCompat.getColor(c, R.color.ShipRed));

        collectible = new Paint();
        collectible.setColor(ContextCompat.getColor(c, R.color.StarYellow));

        exit = new Paint();
        exit.setColor(ContextCompat.getColor(c, R.color.ExitBlue));

        start = new Paint();
        start.setColor(ContextCompat.getColor(c, R.color.StartBlue));
    }

    /**
     * Reaction on size changed. Size of cell will be changed in a way that whole maze will fit into the view.
     * @param w     new width
     * @param h     new height
     * @param oldw  old widht
     * @param oldh  old height
     */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        cellSize = 100;
        this.width = w;
        this.height = h;

        // how big should cells be to fit perfectly
        int[] dimension = maze.getDimension();
        int spaceX = (this.width-40) / (dimension[0] - 1);
        int spaceY = (this.height-40) / (dimension[1] - 1);

        // if current cellsize too big set it to the calculated perfect size
        if (spaceX < cellSize)
            cellSize = spaceX;
        if (spaceY < cellSize)
            cellSize = spaceY;

        invalidate();
    }

    /**
     * Draw current state of maze on canvas
     * @param canvas    canvas
     */
    protected void onDraw(Canvas canvas) {
        int[] p = {0, 0};

        // no maze to draw
        if (maze == null)
            return;

        // size of maze
        int[] dimension = maze.getDimension();
        // cells to draw
        Cell[][] cells = maze.getCells();

        // shift so [0, 0] will be in left top corner of maze
        float shiftX = (width - (dimension[0]-1)*cellSize) / 2.0f;
        float shiftY = (height - (dimension[1]-1)*cellSize) / 2.0f;
        canvas.translate(shiftX, shiftY);

        // draw background
        canvas.drawRect(p[0] - 20, p[1] - 20, cellSize * (dimension[0]-1) + 20, cellSize * (dimension[1]-1) + 20, background);

        // draw cells
        for (int i = 0; i < dimension[1]; i++) {
            for (int j = 0; j < dimension[0]; j++) {

                //background
                if ( (i < dimension[1] - 1) && (j < dimension[0] - 1) ) {
                    //exit
                    if ( (i == maze.endY) && (j == maze.endX) && (maze.open) )
                        drawPortal(canvas, exit, p[0], p[1]);
                    //entry
                    if ( (i == maze.entryY) && (j == maze.entryX) )
                        drawPortal(canvas, start, p[0], p[1]);
                }

                //player
                if (cells[i][j].player)
                    drawTriangle(canvas, character, p[0] + cellSize/2, p[1] + cellSize/2, cellSize);

                //collectible
                if (cells[i][j].collectible)
                    drawStar(canvas, collectible, p[0] + cellSize/2, p[1] + cellSize/2, cellSize);

                p[0] += cellSize;
            }
            p[0] = 0;
            p[1] += cellSize;
        }

        //walls
        p[1] = 0;
        for (int i = 0; i < dimension[1]; i++) {
            for (int j = 0; j < dimension[0]; j++) {
                if ( (j < dimension[0] - 1) && (!cells[i][j].north) )
                    canvas.drawLine(p[0], p[1], p[0] + cellSize, p[1], wall);
                if ( (i < dimension[1] - 1) && (!cells[i][j].west) )
                    canvas.drawLine(p[0], p[1], p[0], p[1] + cellSize, wall);

                p[0] += cellSize;
            }
            p[0] = 0;
            p[1] += cellSize;
        }
    }

    /**
     * Draw portal representing the end or start of a maze
     * @param canvas    canvas
     * @param colour    color of portal
     * @param x         x coordinate of the left top corner of cell
     * @param y         y coordinate of the left top corner of cell
     */
    private void drawPortal(Canvas canvas, Paint colour, int x, int y) {
        canvas.drawRect(x + strokeWidth * adjust, y + strokeWidth * adjust, x + cellSize - strokeWidth, y + cellSize - strokeWidth, colour);
    }

    /**
     * Draw triangle representing the player character in maze. Player will be rotated dependig on the last move
     * @param canvas    canvas
     * @param paint     colour of player
     * @param x         x coordinate of center of cell
     * @param y         y coordinate of  center of cell
     * @param width     width of cell
     */
    public void drawTriangle(Canvas canvas, Paint paint, int x, int y, int width) {
        int halfWidth = width / 2;

        Path path = new Path();
        path.moveTo(x, y - halfWidth + strokeWidth * adjust); // Top
        path.lineTo(x - halfWidth + strokeWidth * adjust, y + halfWidth - strokeWidth * adjust); // Bottom left
        path.lineTo(x + halfWidth - strokeWidth * adjust, y + halfWidth - strokeWidth * adjust); // Bottom right
        path.lineTo(x, y - halfWidth + strokeWidth * adjust); // Back to Top
        path.close();

        // spin so it faces the direction of last move in maze
        Matrix m = new Matrix();
        m.postRotate(maze.turn, x, y);
        path.transform(m);

        canvas.drawPath(path, paint);
    }

    /**
     * Draw star in cell
     * @param canvas    canvas
     * @param paint     color of star
     * @param x         x coordinate of center of cell
     * @param y         y coordinate of center of cell
     * @param width     width of cell
     */
    public void drawStar(Canvas canvas, Paint paint, int x, int y, int width) {
        int halfWidth = width / 3;

        Point vect = new Point();
        Path path = new Path();
        path.moveTo(x, y - halfWidth); // top

        vect.x = halfWidth;
        vect.y = -halfWidth;
        path.lineTo(x + 1/3f * vect.x, y + 1/3f * vect.y); // right

        path.lineTo(x + halfWidth, y); // right

        vect.x = halfWidth;
        vect.y = halfWidth;
        path.lineTo(x + 1/3f * vect.x, y + 1/3f * vect.y); // right

        path.lineTo(x, y + halfWidth); // bottom

        vect.x = -halfWidth;
        vect.y = halfWidth;
        path.lineTo(x + 1/3f * vect.x, y + 1/3f * vect.y); // right

        path.lineTo(x - halfWidth, y); // left

        vect.x = -halfWidth;
        vect.y = -halfWidth;
        path.lineTo(x + 1/3f * vect.x, y + 1/3f * vect.y); // right

        path.lineTo(x, y - halfWidth); // right

        path.close();

        canvas.drawPath(path, paint);
    }

    /**
     * Set new maze to display
     * @param game  maze to display
     */
    public void setGame(Maze game) {
        this.maze = game;
        invalidate();
    }

}
