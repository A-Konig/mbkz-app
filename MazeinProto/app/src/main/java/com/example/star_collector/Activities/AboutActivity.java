package com.example.star_collector.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.WindowManager;

import com.example.star_collector.R;

/**
 * Activity controlling the about layout
 *
 * @author Alex Konig
 */
public class AboutActivity extends AppCompatActivity {

    /**
     * Called on creation of the layout
     * @param savedInstanceState    saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
    }

    /**
     * Called when the back button pressed
     */
    @Override
    public void onBackPressed() {
        finish();
    }
}
