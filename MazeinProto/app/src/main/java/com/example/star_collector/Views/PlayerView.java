package com.example.star_collector.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.example.star_collector.R;

/**
 * Class used in AboutActivity layout for the display of the player character
 *
 * @author Alex Konig
 */
public class PlayerView extends View {

    /** Color of player */
    private Paint character;

    /** Width of view */
    private int width;
    /** Height of view */
    private int height;

    /** Width of stroke */
    private final int strokeWidth = 5;
    /** Default size of one cell in maze */
    private final int cellSize = 100;

    /**
     * Constructor
     * @param context context
     */
    public PlayerView(Context context) {
        this(context, null);
    }

    /**
     * Constructor that sets the colour of player
     * @param c context
     * @param attrs attributes
     */
    public PlayerView(Context c, AttributeSet attrs)
    {
        super(c, attrs);
        character = new Paint();
        character.setColor(ContextCompat.getColor(c, R.color.ShipRed));
    }

    /**
     * Reaction on size changed
     * @param w     new widht
     * @param h     new height
     * @param oldw  old width
     * @param oldh  old height
     */
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width = w;
        this.height = h;
    }

    /**
     * Method that draws contents of the view
     * @param canvas    canvas
     */
    protected void onDraw(Canvas canvas) {
        drawTriangle(canvas, character, width/2, height/2, cellSize);
    }

    /**
     * Draw triangle representing the player character in maze.
     * @param canvas    canvas
     * @param paint     colour of player
     * @param x         x coordinate of center of cell
     * @param y         y coordinate of  center of cell
     * @param width     width of cell
     */
    public void drawTriangle(Canvas canvas, Paint paint, int x, int y, int width) {
        int halfWidth = width / 2;

        Path path = new Path();
        path.moveTo(x, y - halfWidth + strokeWidth * 1.5f); // Top
        path.lineTo(x - halfWidth + strokeWidth * 1.5f, y + halfWidth - strokeWidth * 1.5f); // Bottom left
        path.lineTo(x + halfWidth - strokeWidth * 1.5f, y + halfWidth - strokeWidth * 1.5f); // Bottom right
        path.lineTo(x, y - halfWidth + strokeWidth * 1.5f); // Back to Top
        path.close();

        canvas.drawPath(path, paint);
    }

}
