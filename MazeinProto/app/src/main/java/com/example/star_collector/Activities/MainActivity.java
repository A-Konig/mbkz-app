package com.example.star_collector.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.star_collector.Model.Utils;
import com.example.star_collector.R;

import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Activity controlling the main layout
 *
 * @author Alex Konig
 */
public class MainActivity extends AppCompatActivity {

    /** Is timed mode on */
    boolean timed;
    /** Difficulty (represented by time in seconds, default at 90s) */
    int time = 90;

    /**
     * Called on creation of the layout
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        // background
        View view =  this.getWindow().getDecorView();
        int orientation = getResources().getConfiguration().orientation;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if(width>1600)
                view.setBackgroundResource (R.drawable.titlebgl_xxxhdpi);
            else if(width>960)
                view.setBackgroundResource(R.drawable.titlebgl_xxhdpi);
            else if(width>800)
                view.setBackgroundResource(R.drawable.titlebgl_xhdpi);
            else if(width>480)
                view.setBackgroundResource(R.drawable.titlebgl_hdpi);
            else if (width>320)
                view.setBackgroundResource(R.drawable.titlebgl_mdpi);
            else
                view.setBackgroundResource(R.drawable.titlebgl_ldpi);
        } else {
            if(width>960)
                view.setBackgroundResource(R.drawable.titlebg_xxxhdpi);
            else if(width>640)
                view.setBackgroundResource(R.drawable.titlebg_xxhdpi);
            else if(width>480)
                view.setBackgroundResource(R.drawable.titlebg_xhdpi);
            else if(width>320)
                view.setBackgroundResource(R.drawable.titlebg_hdpi);
            else if (width>240)
                view.setBackgroundResource(R.drawable.titlebg_mdpi);
            else
                view.setBackgroundResource(R.drawable.titlebg_ldpi);
        }

    }

    /**
     * Display dialog with best scores
     * @param v view
     */
    public void SeeBest(View v) {

        // read best scores from disk
        FileInputStream fis;
        int[] saved = {0, 0 , 0};
        byte [] buffer = new byte[1024];
        String FILENAME = "Highscores.txt";

        try {
            fis= openFileInput(FILENAME);
            fis.read(buffer, 0, 1024);
            fis.close();

            String line = new String(buffer);

            String[] vals = line.split(" ");
            saved[0] = Integer.parseInt(vals[0]);
            saved[1] = Integer.parseInt(vals[1]);
            saved[2] = Integer.parseInt(vals[2]);

            System.out.println(saved[0] + " " + saved[1] + " " + saved[2]);
        } catch (Exception e) {
            saved[0] = 0;
        }

        // create displayed text
        String msg = "";
        for (int i = 0; i < 3; i++) {
            msg += saved[i] + " finished mazes\n";
        }

        // create dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg).setTitle("Highscores").setIcon(android.R.drawable.ic_dialog_info);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog a1 = builder.create();
        a1.show();
    }

    /**
     * Display dialog for the selection of difficulty of timed mode
     * @param v view
     */
    public void SelectDifficulty(View v) {
        final Context c1 = v.getContext();
        final TextView diffTXT = (TextView)findViewById(R.id.difficultyTXT);

        // create dilog
        AlertDialog.Builder builder = new AlertDialog.Builder(c1);
        builder.setTitle("Select difficulty");
        builder.setItems(new CharSequence[]
                        {"Easy", "Normal", "Hard"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            // easy
                            case 0:
                                time = 180;
                                diffTXT.setText(R.string.diffEasySTR);
                                Toast.makeText(c1, "Set to Easy", Toast.LENGTH_SHORT).show();
                                break;
                            // normal
                            case 1:
                                time = 90;
                                diffTXT.setText(R.string.diffNormSTR);
                                Toast.makeText(c1, "Set to Normal", Toast.LENGTH_SHORT).show();
                                break;
                            // hard
                            case 2:
                                time = 30;
                                diffTXT.setText(R.string.diffHardSTR);
                                Toast.makeText(c1, "Set to Hard", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
        builder.create().show();
    }

    /**
     * Start game activity or select level activity
     * @param v view
     */
    public void LaunchGame(View v) {
        Switch modeSW = findViewById(R.id.modeSW);
        timed = !modeSW.isChecked();

        // get last open level
        loadOpenLevel();

        Intent i;
        // time mode
        if (timed) {
            // put extra
            i = new Intent(this,  GameActivity.class);
            i.putExtra("LVL", -1);
            i.putExtra("TIME", time);
        // story mode
        } else {
            // put extra
            i = new Intent(this, SelectLvlActivity.class);
            i.putExtra("OPENED", Utils.loadLevel());
        }
        startActivity(i);
    }

    /**
     * Load last open level from disk
     */
    private void loadOpenLevel() {
        FileInputStream fis;
        byte [] buffer = new byte[1024];
        int val;
        String FILENAME = "LastOpen.txt";
        try {
            fis = openFileInput(FILENAME);
            fis.read(buffer, 0, 4);
            fis.close();

            val = Integer.parseInt(""+((new String(buffer)).charAt(0)));
            Utils.changeLevel(val);

        } catch (Exception e) {

            // no file created yet -> create
            FileOutputStream fos;
            String msg = "" + 1;
            try {
                fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                fos.write(msg.getBytes());
                fos.close();
                Utils.changeLevel(1);

            } catch (Exception ex) { System.out.println("CHYBA PŘI ČTENÍ"); }

        }
    }

    /**
     * Start About acitvity
     * @param v  view
     */
    public void ShowHowToPlay(View v) {
        Intent i = new Intent(this,  AboutActivity.class);
        startActivity(i);
    }

    /**
     * Change game mode
     * @param v view
     */
    public void SelectGameMode(View v) {
        timed = !timed;
    }

}
