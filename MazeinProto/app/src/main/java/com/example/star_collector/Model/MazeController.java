package com.example.star_collector.Model;

/**
 * Class providing the manipulation with a maze
 *
 * @author Alex Konig
 */
public class MazeController {
    /** Controlled maze */
    private Maze maze;

    /** Turns per level */
    private int TURNS;

    /** Number of collected collectibles */
	public int collected;
	/** Number of solved mazes */
	private int solved;

    /**
     * Constructor
     * @param maze      maze to controll
     * @param level     selected level
     */
	public MazeController(Maze maze, int level) {
	    this.maze = maze;

	    // timed mode
	    if (level == -1)
	        TURNS = Integer.MAX_VALUE;
        // story mode
	    else
	        TURNS = 3;
    }

    /**
     * Create a new maze
     * @return  false if finished with level, true if new maze created
     */
    public boolean createMaze() {
        solved++;

        // finished level
        if (solved > TURNS)
            return false;

        // timed mode -> change maze dimensions
        if (TURNS == Integer.MAX_VALUE)
            maze.changeDimension();

        // generate new maze
        maze.build();
        return true;
    }

    /**
     * Moves player and picks up collectibles from the cell the player is moved to, if there are any.
     * @param where	where to move the player (1 - left, 2 - down, 3 - right, 5 - up)
     */
    public void movePlayer(int where) {
        int[] dimension = maze.getDimension();

        // move and turn player, collect collectibles
        maze.maze[maze.playerY][maze.playerX].player = false;
        switch (where) {
            // up
            case 5:
                if ( (maze.playerY-1 >= 0) && (maze.maze[maze.playerY][maze.playerX].north) ) {
                    maze.playerY -= 1;
                    maze.turn = 0;
                    if (maze.collect())
                        collected++;
                }
                break;
            // left
            case 1:
                if ( (maze.playerX-1 >= 0) && (maze.maze[maze.playerY][maze.playerX].west) ) {
                    maze.playerX -= 1;
                    maze.turn = -90;
                    if (maze.collect())
                        collected++;
                }
                break;
            // down
            case 2:
                if ( (maze.playerY+1 < dimension[1]-1) && (maze.maze[maze.playerY+1][maze.playerX].north) ) {
                    maze.playerY += 1;
                    maze.turn = 180;
                    if (maze.collect())
                        collected++;
                }
                break;
            // right
            case 3:
                if ( (maze.playerX+1 < dimension[0]-1) && (maze.maze[maze.playerY][maze.playerX+1].west) ) {
                    maze.playerX += 1;
                    maze.turn = 90;
                    if (maze.collect())
                        collected++;
                }
                break;
        }

        maze.maze[maze.playerY][maze.playerX].player = true;
    }

}