package com.example.star_collector.Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Stack;

/**
 * Class representing the maze
 * 
 * @author Alex Konig
 */
public class Maze {
	/** Number of collectables */
	private int COLL_COUNT;

	/** Width of the maze */
	private int width;
	/** Height of the maze */
	private int heigth;

	/** Array representing the cells of the maze */
	Cell[][] maze;
	
	/** X coordinate of the player */
	public int playerX;
	/** Y coordinate of the player */
	public int playerY;
	/** Where is the player facing */
	public int turn;
	
	/** X coordinate of the entry */
	public int entryX;
	/** Y coordinate of the entry */
	public int entryY;

	/** X coordinate of the exit */
	public int endX;
	/** Y coordinate of the exit */
	public int endY;

	/** Is the exit open */
	public boolean open;
	/** Number of collected collectibles */
	private int collected;

	/**
	 * Constructor
	 * @param lvl	level of maze
	 */
	public Maze(int lvl) {
		switch (lvl) {
			// random
			case -1:
				Random r = new Random();
				this.width = r.nextInt(11 - 4) + 5;
				this.heigth = r.nextInt(8 - 4) + 5;
				COLL_COUNT = 0;
				break;
			// level 1, 5x5
			case 1:
				this.width = 5;
				this.heigth = 5;
				COLL_COUNT = (int) (1 / 16.0 * (width * heigth));
				break;
			// level 2, 7x6
			case 2:
				this.width = 7;
				this.heigth = 6;
				COLL_COUNT = (int) (1 / 16.0 * (width * heigth));
				break;
			// level 3, 9x7
			case 3:
				this.width = 9;
				this.heigth = 7;
				COLL_COUNT = (int) (1 / 16.0 * (width * heigth));
				break;
			// level 4, 11x8
			case 4:
				this.width = 11;
				this.heigth = 8;
				COLL_COUNT = (int) (1 / 16.0 * (width * heigth));
				break;
		}

		this.width++;
		this.heigth++;

		maze = new Cell[this.heigth][this.width];
	}

	/**
	 * Method building the maze, creating exit and entry to the maze. Generating the positions of the collectibles,
	 * and positioning the player at the start.
	 */
	void build() {

		collected = 0;
		open = false;

		// create cells
		for (int i = 0; i < this.heigth; i++) {
			for (int j = 0; j < this.width; j++) {
				maze[i][j] = new Cell(j, i);
			}
		}

		Random r = new Random();
		int index = r.nextInt(heigth-1);

		//player
		playerX = 0; playerY = index;

		//entry to the maze
		entryX = 0; entryY = index;

		maze[index][0].west = true;
		maze[index][0].player = true;

		//end of the maze
		index = r.nextInt(heigth-1);
		endX = width-2; endY = index;

		//collectibles
		int[] collectiblesX = new int[COLL_COUNT];
		int[] collectiblesY = new int[COLL_COUNT];

		// set collectibles
		Arrays.fill(collectiblesX, -1);
		Arrays.fill(collectiblesY, -1);
		for (int i = 0; i < COLL_COUNT; i++) {
			boolean cont = false;

			int indexX = 0;
			int indexY = 0;
			do {
				cont = false;
				indexX = r.nextInt(width-1);
				indexY = r.nextInt(heigth-1);

				for (int j = 0; j < COLL_COUNT; j++) {
					if ( ( (collectiblesX[j] == indexX) && (collectiblesY[j] == indexY) ) ||
						 ( (indexX == entryX) && (indexY == entryY) ) ) {
						cont = true;
					}
				}
			} while (cont);

			collectiblesX[i] = indexX;
			collectiblesY[i] = indexY;

			maze[indexY][indexX].collectible = true;
		}

		//if no collectibles
		if (COLL_COUNT == 0) {
			open();
		}

		generatePath();
	}

	//generating path
	private void generatePath() {
		Random r = new Random();

		boolean visited = true;
		Cell curr = maze[playerY][playerX];
		curr.visited = true;
		Stack<Cell> stack = new Stack<>();

		while (visited) {
			visited = countVisited();

			ArrayList<Cell> neighbours = getUnvisitedNeighbours(curr);

			if (neighbours.size() > 0) {
				Cell newCurr = neighbours.get(r.nextInt(neighbours.size()));

				//left
				if (newCurr.x+1 == curr.x) {
					curr.west = true;
				}
				//right
				if (newCurr.x-1 == curr.x) {
					newCurr.west = true;
				}
				//up
				if (newCurr.y+1 == curr.y) {
					curr.north = true;
				}
				//down
				if (newCurr.y-1 == curr.y) {
					newCurr.north = true;
				}

				stack.add(curr);
				curr = newCurr;
				curr.visited = true;
			} else if (stack.size() > 0) {
				curr = stack.pop();
			}
		}
	}

	/**
	 * Gets unvisited neighbors of a cell
	 * @param curr	cell in question
	 * @return 		list of unvisited cells
	 */
	private ArrayList<Cell> getUnvisitedNeighbours(Cell curr) {
		ArrayList<Cell> result = new ArrayList<>();
		
		//top
		if ( (curr.y-1 >= 0) && (!maze[curr.y-1][curr.x].visited) )
				result.add(maze[curr.y-1][curr.x]);

		//bottom
		if ( (curr.y+1 < heigth-1) && (!maze[curr.y+1][curr.x].visited) )
			result.add(maze[curr.y+1][curr.x]);

		//left
		if ( (curr.x-1 >= 0) && (!maze[curr.y][curr.x-1].visited) )
			result.add(maze[curr.y][curr.x-1]);

		//right
		if ( (curr.x+1 < width-1) && (!maze[curr.y][curr.x+1].visited) )
			result.add(maze[curr.y][curr.x+1]);

		return result;
	}

	/**
	 * Are there any unvisited cells
	 * @return numver of unvisited cells
	 */
	private boolean countVisited() {
		int count = 0;
		for (int i = 0; i < this.heigth-1; i++) {
			for (int j = 0; j < this.width-1; j++) {
				if (!maze[i][j].visited)
					count++;
			}
		}

		return count > 0;
	}

	/**
	 * Picks up a collectible from the position of the player
	 * @return True if collected a new collectible
	 */
	boolean collect() {
		boolean done = false;
		if (maze[playerY][playerX].collectible) {
			collected++;
			maze[playerY][playerX].collectible = false;
			done = true;
		}

		// all collectibles picked up -> open end
		if (collected == COLL_COUNT)
			open();

		return done;
	}

	/**
	 * Opens the exit of the maze
	 */
	private void open() {
		open = true;
		maze[endY][endX+1].west = true;
	}

	/**
	 * Get dimensions of maze
	 * @return	dimensions of the maze in array [width, height]
	 */
	public int[] getDimension() {
		return new int[] {width, heigth};
	}

	/**
	 * Return all cells of maze
	 * @return	array of cells
	 */
	public Cell[][] getCells() {
		return maze;
	}

	/**
	 * Change dimensions of the maze
	 */
	void changeDimension() {
		Random r = new Random();

		this.width = r.nextInt(11 - 4) + 6;
		this.heigth = r.nextInt(8 - 4) + 6;

		COLL_COUNT = 0;
		maze = new Cell[this.heigth][this.width];
	}
}
