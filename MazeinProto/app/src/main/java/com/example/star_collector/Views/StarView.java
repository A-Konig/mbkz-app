package com.example.star_collector.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.example.star_collector.R;

/**
 * Class used in AboutActivity layout for the display of a star
 *
 * @author Alex Konig
 */
public class StarView extends View {

    /** Color of star */
    private Paint collectible;

    /** Width of view */
    private int width;
    /** Height of view */
    private int height;

    /** Default size of one cell in maze */
    private int cellSize = 100;

    /**
     * Constructor
     * @param context context
     */
    public StarView(Context context) {
        this(context, null);
    }

    /**
     * Constructor that sets the colour of star
     * @param c context
     * @param attrs attributes
     */
    public StarView(Context c, AttributeSet attrs)
    {
        super(c, attrs);
        collectible = new Paint();
        collectible.setColor(ContextCompat.getColor(c, R.color.StarYellow));
    }

    /**
     * Reaction on size changed
     * @param w     new widht
     * @param h     new height
     * @param oldw  old width
     * @param oldh  old height
     */
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width = w;
        this.height = h;
    }

    /**
     * Method that draws contents of the view
     * @param canvas    canvas
     */
    protected void onDraw(Canvas canvas) {
        drawStar(canvas, collectible, width/2, height/2, cellSize);
    }

    /**
     * Draw star in cell
     * @param canvas    canvas
     * @param paint     color of star
     * @param x         x coordinate of center of cell
     * @param y         y coordinate of center of cell
     * @param width     width of cell
     */
    public void drawStar(Canvas canvas, Paint paint, int x, int y, int width) {
        int halfWidth = width / 3;


        Point vect = new Point();

        Path path = new Path();
        path.moveTo(x, y - halfWidth);

        vect.x = halfWidth;
        vect.y = -halfWidth;
        path.lineTo(x + 1/3f * vect.x, y + 1/3f * vect.y); // right

        path.lineTo(x + halfWidth, y); // right

        vect.x = halfWidth;
        vect.y = halfWidth;
        path.lineTo(x + 1/3f * vect.x, y + 1/3f * vect.y); // right

        path.lineTo(x, y + halfWidth); // bottom

        vect.x = -halfWidth;
        vect.y = halfWidth;
        path.lineTo(x + 1/3f * vect.x, y + 1/3f * vect.y); // right

        path.lineTo(x - halfWidth, y); // left

        vect.x = -halfWidth;
        vect.y = -halfWidth;
        path.lineTo(x + 1/3f * vect.x, y + 1/3f * vect.y); // right

        path.lineTo(x, y - halfWidth); // right

        path.close();

        canvas.drawPath(path, paint);
    }

}
